# Code Challenge - Thermostat

A simple rails api that saves readings for thermostats.

### To make the app run the following commands:

```
bundle install
rake db:create
rake db:seed
rails s
```

### To run the tests:

```
rspec
```

### We have the following endpoints, followed by some examples on how to call them via curl:

POST #create (To create a reading)
```
curl -X POST -d "household_token=<token>&reading[temperature]=14&reading[humidity]=14.3&reading[battery_charge]=75.2" localhost:3000/readings
```

GET #show (To get an specific reading)
```
curl -X GET localhost:3000/readings/<tracking_number>?household_token=<token>
```

GET #stats (To get stats given a certain thermostat token)
```
curl -X GET localhost:3000/readings/stats?household_token=<token>
```
