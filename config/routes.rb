Rails.application.routes.draw do
  resources :readings, only: %i[show create], defaults: { format: :json } do
    get :stats, on: :collection
  end
end
