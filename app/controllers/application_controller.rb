class ApplicationController < ActionController::API
  before_action :authenticate_thermostat
  def authenticate_thermostat
    @thermostat = Thermostat.find_by_household_token! params[:household_token]
  rescue
    render json: {error: "Thermostat Not Found"}, status: 404
  end
end
