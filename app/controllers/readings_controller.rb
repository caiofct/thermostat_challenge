class ReadingsController < ApplicationController
  # GET /readings/1
  def show
    @reading = Rails.cache.fetch "#{@thermostat.household_token}/#{params[:id]}" do
      @thermostat.readings.find_by_tracking_number! params[:id]
    end
  rescue
    render json: {error: "Reading not found"}, status: 404
  end

  # POST /readings
  def create
    @reading = Reading.new(reading_params)
    @reading.thermostat_id = @thermostat.id
    # just to make the validation pass
    @reading.tracking_number = 1
    if @reading.valid?
      @reading.tracking_number = Reading.last_tracking_by_house(@thermostat.household_token) + 1
      # caching the reading to allow for fast response when fetching it's data
      Rails.cache.write("#{@thermostat.household_token}/#{@reading.tracking_number}", @reading)
      # saving the reading in the actual database in background using ActiveJob + sidekiq
      SaveReadingJob.perform_later(@reading.tracking_number, @thermostat.id, reading_params)
      render json: { tracking_number: @reading.tracking_number }, status: :created
    else
      render json: @reading.errors, status: :unprocessable_entity
    end
  end

  # GET /stats
  def stats
    @temperature_min = @thermostat.readings.minimum(:temperature)
    @temperature_max = @thermostat.readings.maximum(:temperature)
    @temperature_average = @thermostat.readings.average(:temperature)

    @humidity_min = @thermostat.readings.minimum(:humidity)
    @humidity_max = @thermostat.readings.maximum(:humidity)
    @humidity_average = @thermostat.readings.average(:humidity)

    @battery_charge_min = @thermostat.readings.minimum(:battery_charge)
    @battery_charge_max = @thermostat.readings.maximum(:battery_charge)
    @battery_charge_average = @thermostat.readings.average(:battery_charge)
  end

  private
    # Only allow a trusted parameter "white list" through.
    def reading_params
      params.require(:reading).permit(:temperature, :humidity, :battery_charge)
    end
end
