class SaveReadingJob < ApplicationJob
  queue_as :default
  
  def perform(tracking_number, thermostat_id, reading_params)
    reading = Reading.new(reading_params)
    reading.thermostat_id = thermostat_id
    reading.tracking_number = tracking_number
    reading.save
  end
end
