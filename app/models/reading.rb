class Reading < ApplicationRecord
  belongs_to :thermostat

  validates :thermostat_id, presence: true
  validates :tracking_number, presence: true, numericality: { only_integer: true, greater_than: 0 }
  validates :temperature, presence: true, numericality: { greater_than_or_equal_to: 0 }
  validates :humidity, presence: true, numericality: { greater_than_or_equal_to: 0 }
  validates :battery_charge, presence: true, numericality: { greater_than: 0, less_than_or_equal_to: 100 }

  def self.last_tracking_by_house household_token
    last_reading_by_tracking = Reading.joins(:thermostat).where("thermostats.household_token = ?", household_token).order(tracking_number: :desc).first
    last_reading_by_tracking.blank? ? 0 : last_reading_by_tracking.tracking_number
  end
end
