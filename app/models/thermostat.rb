class Thermostat < ApplicationRecord
  has_many :readings

  validates :household_token, presence: true, uniqueness: true
  validates :location, presence: true
end
