json.temperature do
  json.min @temperature_min
  json.max @temperature_max
  json.average @temperature_average
end

json.humidity do
  json.min @humidity_min
  json.max @humidity_max
  json.average @humidity_average
end

json.battery_charge do
  json.min @battery_charge_min
  json.max @battery_charge_max
  json.average @battery_charge_average
end
