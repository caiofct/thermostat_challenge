require 'rails_helper'

RSpec.describe Reading, type: :model do
  fixtures :all

  context 'validation' do
    subject { Reading.new }

    it 'checks associations' do
      is_expected.to belong_to(:thermostat)
    end

    it 'validates' do
      is_expected.to validate_presence_of(:thermostat_id)
      is_expected.to validate_presence_of(:tracking_number)
      is_expected.to validate_presence_of(:temperature)
      is_expected.to validate_presence_of(:humidity)
      is_expected.to validate_presence_of(:battery_charge)
      is_expected.to validate_numericality_of(:tracking_number)
      is_expected.to validate_numericality_of(:temperature)
      is_expected.to validate_numericality_of(:humidity)
      is_expected.to validate_numericality_of(:battery_charge)
    end
  end

  context 'class methods' do
    subject(:reading) { readings(:two) }

    it 'returns the last_tracking_by_house' do
      expect(reading.tracking_number).to eq Reading.last_tracking_by_house(thermostats(:one).household_token)
    end
  end
end
