require 'rails_helper'

RSpec.describe Thermostat, type: :model do
  context 'validation' do
    subject { Thermostat.new }

    it 'checks associations' do
      is_expected.to have_many(:readings)
    end

    it 'validates' do
      is_expected.to validate_presence_of(:household_token)
      is_expected.to validate_uniqueness_of(:household_token)
      is_expected.to validate_presence_of(:location)
    end
  end
end
