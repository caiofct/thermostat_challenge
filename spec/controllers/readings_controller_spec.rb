require "rails_helper"

RSpec.describe ReadingsController, type: :controller do
  fixtures :all
  let(:reading) { readings(:two) }
  let(:first_reading) { readings(:one) }

  describe "GET #show" do
    context "reading exists" do
      before { get :show, params: { id: reading.tracking_number, household_token: reading.thermostat.household_token }, format: :json }

      it "returns success" do
        expect(assigns(:reading).class).to be Reading
        expect(response).to have_http_status(200)
      end
    end

    context "reading do not exist" do
      before { get :show, params: { id: -1, household_token: reading.thermostat.household_token } }

      it "returns 404" do
        expect(response).to have_http_status(404)
      end
    end
  end

  describe "POST #create" do
    context "success" do
      before do
        post :create, params: { household_token: reading.thermostat.household_token, reading: { temperature: 30.5, humidity: 10.2, battery_charge: 58.5 } }
      end

      it "returns success" do
        expect(response).to have_http_status(201)
        parsed_response = JSON.parse(response.body)
        expect(parsed_response['tracking_number']).to eq reading.tracking_number + 1
      end
    end

    context "error" do
      before do
        post :create, params: { household_token: reading.thermostat.household_token, reading: { temperature: 30.5, battery_charge: 58.5 } }
      end

      it "fails to validate" do
        expect(response).to have_http_status(422)
        expect(assigns(:reading).errors.size).to be > 0
      end
    end
  end

  describe "GET #stats" do
    render_views
    before { get :stats, params: { household_token: reading.thermostat.household_token }, format: :json }

    it "returns success" do
      expect(response).to have_http_status(200)
      parsed_response = JSON.parse(response.body)
      expect(parsed_response["temperature"]["min"].to_f).to eq first_reading.temperature
      expect(parsed_response["temperature"]["max"].to_f).to eq reading.temperature
      expect(parsed_response["temperature"]["average"].to_f).to eq (first_reading.temperature + reading.temperature) / 2.0

      expect(parsed_response["humidity"]["min"].to_f).to eq first_reading.humidity
      expect(parsed_response["humidity"]["max"].to_f).to eq reading.humidity
      expect(parsed_response["humidity"]["average"].to_f).to eq (first_reading.humidity + reading.humidity) / 2.0

      expect(parsed_response["battery_charge"]["min"].to_f).to eq first_reading.battery_charge
      expect(parsed_response["battery_charge"]["max"].to_f).to eq reading.battery_charge
      expect(parsed_response["battery_charge"]["average"].to_f).to eq (first_reading.battery_charge + reading.battery_charge) / 2.0
    end
  end
end
